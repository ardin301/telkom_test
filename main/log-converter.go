package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"

	h "log-converter/helper"
	l "log-converter/log"
)

func main() {

	//extract all param necessary for this program to ran
	flag.Usage = func() {
		fmt.Printf("Usage: %s [options] <example.log>\n", filepath.Base(os.Args[0]))
		fmt.Println("example.log :")
		fmt.Println("must be .log file")
		fmt.Println("with a format = yyyy/MM/dd hh:mm:ss [log_level] pid#tid: *cid error_message")
		fmt.Println("Options:")
		flag.PrintDefaults()
	}

	tp := flag.String("t", "text", "file extension type [text|json]")
	op := flag.String("o", "", "file path of the output, extension must match file extension param e.g: this/path/to/output.txt")

	flag.Parse()
	fp := flag.Arg(0)

	log.Println("Converting...")
	//validate all the param
	o, err := h.Validate(fp, tp, op)
	if err != nil {
		h.ErrExit("error while validating the input", err)
	}

	//open the file that needs to be converted
	f, err := os.Open(fp)
	if err != nil {
		h.ErrExit("error while opening file", err)
	}
	defer f.Close()

	//read the file line per line
	scanner := bufio.NewScanner(f)
	scanner.Split(bufio.ScanLines)

	count := 1
	var logs []l.Log
	for scanner.Scan() {

		//error prefix to make error tracing on the file easier
		errPrefix := "error on line " + strconv.Itoa(count) + " :"

		//convert the text to log
		newData, err := l.ConvertLog(scanner.Text())
		if err != nil {
			h.ErrExit(errPrefix, err)
		}

		//add to list and add increment for the error prefix
		logs = append(logs, newData)
		count++
	}

	//save the file
	err = h.SaveFile(logs, o)
	if err != nil {
		h.ErrExit("error while saving the file", err)
	}

	log.Println("Done")
}

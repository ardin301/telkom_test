package converter

import (
	"errors"
	"strconv"
	"strings"
	"time"
)

type Log struct {
	TimeStamp time.Time `json:"timeStamp"`
	LogLevel  string    `json:"logLevel"`
	PID       int       `json:"pid"`
	TID       int       `json:"tid"`
	CID       int       `json:"cid"`
	Message   string    `json:"message"`
}

func ConvertTimeStamp(rawDate string) (time.Time, error) {
	timeStamp, err := time.Parse("2006/01/02 15:04:05", rawDate)
	if err != nil {
		return time.Time{}, errors.New("timestamp format is invalid " + err.Error())
	}
	return timeStamp, nil
}

func ConvertLogLevel(rawLogLevel string) (string, error) {
	if !strings.HasPrefix(rawLogLevel, "[") || !strings.HasSuffix(rawLogLevel, "]") {
		return "", errors.New("log level format is invalid" + "| should be [log_level] format")
	}
	idx := strings.Index(rawLogLevel, "]")
	return rawLogLevel[1:idx], nil
}

func SplitPIDandTID(rawStr string) (string, string, error) {
	if strings.Count(rawStr, "#") != 1 || strings.HasPrefix(rawStr, "#") || strings.HasSuffix(rawStr, "#") {
		return "", "", errors.New("pid & tid format is invalid " + "| should be pid#tid format")
	}
	arrStr := strings.Split(rawStr, "#")
	return arrStr[0], arrStr[1], nil
}

func ConvertPID(rawPID string) (int, error) {
	pid, err := strconv.Atoi(rawPID)
	if err != nil {
		return 0, errors.New("pid is invalid " + err.Error())
	}
	return pid, nil
}

func ConvertTID(rawTID string) (int, error) {
	idx := strings.Index(rawTID, ":")
	if idx == -1 {
		return 0, errors.New("tid is invalid " + "| should be tid: format")
	}
	t := rawTID[:idx]
	tid, err := strconv.Atoi(t)
	if err != nil {
		return 0, errors.New("tid is invalid " + err.Error())
	}
	return tid, nil
}

func ConvertCID(rawCID string) (int, error) {
	if !strings.ContainsAny(rawCID, "*") {
		return 0, errors.New("cid is invalid " + "| should be *cid format")
	}
	c := rawCID[1:]
	cid, err := strconv.Atoi(c)
	if err != nil {
		return 0, errors.New("cid is invalid " + err.Error())
	}

	return cid, nil
}

func ConvertLog(rawStr string) (Log, error) {
	strArr := strings.Split(rawStr, " ")

	//check if all the positional argument is present
	if len(strArr) < 6 {
		return Log{}, errors.New("one of the field is missing")
	}

	var newData Log

	//timestamp
	rawDate := strings.Join(strArr[:2], " ")
	timeStamp, err := ConvertTimeStamp(rawDate)
	if err != nil {
		return Log{}, err
	}
	newData.TimeStamp = timeStamp
	strArr = strArr[2:]

	//log level
	logLevel, err := ConvertLogLevel(strArr[0])
	if err != nil {
		return Log{}, err
	}
	newData.LogLevel = logLevel
	strArr = strArr[1:]

	//PID & TID
	p, t, err := SplitPIDandTID(strArr[0])
	if err != nil {
		return Log{}, err
	}

	//PID
	newData.PID, err = ConvertPID(p)
	if err != nil {
		return Log{}, err
	}

	//TID
	newData.TID, err = ConvertTID(t)
	if err != nil {
		return Log{}, err
	}
	strArr = strArr[1:]

	//CID
	newData.CID, err = ConvertCID(strArr[0])
	if err != nil {
		return Log{}, err
	}
	strArr = strArr[1:]

	//Message
	newData.Message = strings.Join(strArr, " ")

	return newData, nil
}

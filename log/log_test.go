package converter

import "testing"

func TestConvertTimeStamp(t *testing.T) {
	tests := []struct {
		name  string
		field string
		resp  bool
	}{
		{
			"Testcase 1: valid date",
			"2006/01/02 15:04:05",
			true,
		},
		{
			"Testcase 2: invalid date",
			"2006-01-02T15:04:05",
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			_, err := ConvertTimeStamp(test.field)
			if (err == nil && !test.resp) || (err != nil && test.resp) {
				t.Error("got error : ", err)
			}
		})
	}
}

func TestConvertLogLevel(t *testing.T) {
	tests := []struct {
		name  string
		field string
		resp  bool
	}{
		{
			"Testcase 1: valid log level",
			"[error]",
			true,
		},
		{
			"Testcase 2: invalid log level",
			"error",
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			_, err := ConvertLogLevel(test.field)
			if (err == nil && !test.resp) || (err != nil && test.resp) {
				t.Error("got error : ", err)
			}
		})
	}
}

func TestSplitPIDandTID(t *testing.T) {
	tests := []struct {
		name  string
		field string
		resp  bool
	}{
		{
			"Testcase 1: valid combination of pid and tid",
			"123#345:",
			true,
		},
		{
			"Testcase 2: invalid combination (pound doesnt exist)",
			"123345:",
			false,
		},
		{
			"Testcase 2: invalid combination (pound doesnt exist)",
			"123345:",
			false,
		},
		{
			"Testcase 3: invalid combination (pound in prefix)",
			"#123345:",
			false,
		},
		{
			"Testcase 4: invalid combination (pound in suffix)",
			"123345:#",
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			_, _, err := SplitPIDandTID(test.field)
			if (err == nil && !test.resp) || (err != nil && test.resp) {
				t.Error("got error : ", err)
			}
		})
	}
}

func TestConvertPID(t *testing.T) {
	tests := []struct {
		name  string
		field string
		resp  bool
	}{
		{
			"Testcase 1: valid pid",
			"123",
			true,
		},
		{
			"Testcase 2: invalid pid",
			"abc",
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			_, err := ConvertPID(test.field)
			if (err == nil && !test.resp) || (err != nil && test.resp) {
				t.Error("got error : ", err)
			}
		})
	}
}

func TestConvertTID(t *testing.T) {
	tests := []struct {
		name  string
		field string
		resp  bool
	}{
		{
			"Testcase 1: valid tid",
			"123:",
			true,
		},
		{
			"Testcase 2: invalid pid (no semicolon)",
			"123",
			false,
		},

		{
			"Testcase 2: invalid pid (not a number)",
			"abc:",
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			_, err := ConvertTID(test.field)
			if (err == nil && !test.resp) || (err != nil && test.resp) {
				t.Error("got error : ", err)
			}
		})
	}
}

func TestConvertCID(t *testing.T) {
	tests := []struct {
		name  string
		field string
		resp  bool
	}{
		{
			"Testcase 1: valid cid",
			"*123",
			true,
		},
		{
			"Testcase 2: invalid cid (no asterisk)",
			"123",
			false,
		},

		{
			"Testcase 2: invalid cid (not a number)",
			"*abc",
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			_, err := ConvertCID(test.field)
			if (err == nil && !test.resp) || (err != nil && test.resp) {
				t.Error("got error : ", err)
			}
		})
	}
}

func TestConvertLog(t *testing.T) {
	tests := []struct {
		name  string
		field string
		resp  bool
	}{
		{
			"Testcase 1: valid log",
			"2017/08/01 01:05:50 [error] 28148#28148: *154 FastCGI sent in stderr",
			true,
		},
		{
			"Testcase 2: invalid log",
			"2017/08/01 01:05:50 [error] 28148#28148: *154",
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			_, err := ConvertLog(test.field)
			if (err == nil && !test.resp) || (err != nil && test.resp) {
				t.Error("got error : ", err)
			}
		})
	}
}

package helper

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

const (
	defaultName = "error"
)

var fileExts = map[string]string{
	"text": ".txt",
	"json": ".json",
}

func Validate(fp string, t *string, o *string) (string, error) {
	if fp == "" {
		return "", errors.New("need filepath to be converted")
	}

	if fileExt := filepath.Ext(fp); fileExt != ".log" {
		return "", errors.New("this program only accept .log file extension")
	}

	if val, ok := fileExts[*t]; !ok {
		var acceptable string
		for key := range fileExts {
			acceptable += key + ", "
		}
		acceptable = acceptable[:len(acceptable)-2]
		return "", errors.New("this program only return " + acceptable + " file type")
	} else {
		// initialize file extension if the param did not specify the output
		if *o == "" {
			*o = defaultName
			*o += val
		}
	}

	if oExt := filepath.Ext(*o); !((oExt == ".txt" && *t == "text") || (oExt == ".json" && *t == "json")) {
		return "", errors.New("output extension did not match file extension param")
	}

	if _, err := os.Stat(fp); err != nil {
		return "", errors.New("error while retrieving filepath " + err.Error())
	}
	return *o, nil
}

func ErrExit(line string, str error) {
	log.Println("Failed Converting")
	log.Fatalln(line, "|", str)
}

func SaveFile(v interface{}, o string) error {
	//parse to json
	rawData, err := json.MarshalIndent(v, "   ", "   ")
	if err != nil {
		return errors.New("error while parsing to json " + err.Error())
	}

	//save to file
	if strings.HasPrefix(o, "/") || strings.HasPrefix(o, "\\") {
		o = o[1:]
	}

	if dir := filepath.Dir(o); dir != "." {
		os.MkdirAll(dir, 0700)
	}

	err = ioutil.WriteFile(o, rawData, 0644)
	if err != nil {
		return errors.New("error while saving to file " + err.Error())
	}
	return nil
}

package helper

import (
	"os"
	"strings"
	"testing"
)

func TestValidate(t *testing.T) {
	type field struct {
		fp string
		t  string
		o  string
	}

	type resp struct {
		isError bool
		o       string
	}

	tests := []struct {
		name  string
		field field
		resp  resp
	}{
		{
			"Testcase 1: valid param",
			field{"testing/error.log", "json", "error.json"},
			resp{true, "error.json"},
		},
		{
			"Testcase 2: valid param (not filling the output)",
			field{"testing/error.log", "json", ""},
			resp{true, "error.json"},
		},
		{
			"Testcase 3: valid param (using backslash)",
			field{"testing\\error.log", "json", ""},
			resp{true, "error.json"},
		},
		{
			"Testcase 4: invalid param (empty file)",
			field{"", "json", "error.json"},
			resp{false, ""},
		},
		{
			"Testcase 5: invalid param (file extension of file invalid)",
			field{"error.json", "json", "error.json"},
			resp{false, ""},
		},
		{
			"Testcase 6: invalid param (file extension desired is not on the list)",
			field{"testing/error.log", "jpeg", "error.json"},
			resp{false, ""},
		},
		{
			"Testcase 7: invalid param (file extension desired does not match with output extension)",
			field{"testing/error.log", "json", "error.txt"},
			resp{false, ""},
		},
		{
			"Testcase 8: invalid param (file doesnt exist)",
			field{"error.log", "json", "error.json"},
			resp{false, ""},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			o, err := Validate(test.field.fp, &test.field.t, &test.field.o)
			if (err == nil && !test.resp.isError) || (err != nil && test.resp.isError) {
				t.Error("got error : ", err)
			}

			if o != test.resp.o {
				t.Error("output is :", o, "should be :", test.resp.o)
			}
		})
	}
}

func CleanTestSaveFile() {
	os.RemoveAll("test_save_file.json")
	os.RemoveAll("test_save_folder")
	os.RemoveAll("test_save_folder2")
	os.RemoveAll("test_save_folder3")
	os.RemoveAll("test_save_folder4")
}

func TestSaveFile(t *testing.T) {
	CleanTestSaveFile()

	sampleData := []string{"test 1", "test 2"}
	tests := []struct {
		name  string
		field string
		resp  bool
	}{
		{
			"Testcase 1: save file",
			"test_save_file.json",
			true,
		},
		{
			"Testcase 2: save file with path",
			"test_save_folder/test_save_file.json",
			true,
		},
		{
			"Testcase 3: save file with path (backslash)",
			"test_save_folder2\\test_save_file.json",
			true,
		},
		{
			"Testcase 4: save file with path",
			"/test_save_folder3/test_save_file.json",
			true,
		},
		{
			"Testcase 5: save file with path (backslash)",
			"/test_save_folder4\\test_save_file.json",
			true,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			err := SaveFile(sampleData, test.field)
			if (err == nil && !test.resp) || (err != nil && test.resp) {
				t.Error("got error : ", err)
			}

			//check file exist
			if strings.HasPrefix(test.field, "/") || strings.HasPrefix(test.field, "\\") {
				test.field = test.field[1:]
			}
			if _, err := os.Stat(test.field); os.IsNotExist(err) {
				t.Error("file not exist after saving")
			}
		})
	}

	CleanTestSaveFile()
}
